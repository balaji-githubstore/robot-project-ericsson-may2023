*** Settings ***
Library     DatabaseLibrary

Suite Setup     Connect To Database     dbapiModuleName=pymysql     dbName=dbfree_db    dbUsername=dbfree_db
    ...  dbPassword=12345678     dbHost=db4free.net     dbPort=3306

Suite Teardown      Disconnect From Database

*** Comments ***
Table - Products
Columns - product_id, ProductName, description)

*** Test Cases ***
TC1
    ${row_count}     Row Count    select * from Products
    Log To Console    ${row_count}

TC2
    Row Count Is Equal To X    select * from Products    153
    Row Count Is Less Than X    select * from Products    200
    Row Count Is Greater Than X    select * from Products    150
    Row Count Is 0    select * from Products where product_id=7887878787

TC3
    ${desc}     Description    select * from Products
    Log To Console    ${desc}

TC4
    @{output}   DatabaseLibrary.Query    select * from Products
    Log    ${output}
    Log Many    ${output}
    Log    ${output}[0]
    Log    ${output}[0][0]
   
    ${size}     Get Length    ${output}
    Log    ${size}

    FOR    ${i}    IN RANGE    0    ${size}
        Log    ${output}[${i}][0]
    END

TC5
    @{output}   DatabaseLibrary.Query    select * from Products where product_id=995678
    Log    ${output}
    Log    ${output}[0][0]
    Log    ${output}[0][1]
    Log    ${output}[0][2]


TC6 Delete
    DatabaseLibrary.Execute Sql String    Delete from Products where product_id=995678
    Row Count Is 0    select * from Products where product_id=995678
    Check If Not Exists In Database    select * from Products where product_id=995678

TC7 
    Execute Sql String    Insert into Products (product_id, ProductName, description) values ('88778','bala','trainer')
    Row Count Is Equal To X    select * from Products where product_id=88778    1
    