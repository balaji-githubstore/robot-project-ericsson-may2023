*** Settings ***
Library     ../4__python_concepts/area.py


*** Test Cases ***
TC1
    Log To Console    ${e}
    ${res}      Area Of Circle    10
    Log To Console    ${res}
    ${res}      Area Of Rectange    length=4    width=4
    Log To Console    ${res}

TC3
    @{fruits}   Create List     Mango   apple   orange

    FOR    ${i}    IN RANGE    0    3
        Log    ${fruits}[${i}]
    END

TC3
    @{fruits}   Create List     Mango   apple   orange

    FOR    ${fruit}    IN    @{fruits}
        Log    ${fruit}
    END