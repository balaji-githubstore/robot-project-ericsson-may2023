*** Settings ***
Library     RequestsLibrary


*** Test Cases ***
TC1 Find Valid Pet By Id
    Create Session    alias=abc    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=abc   url=pet/5       expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Integers    ${response.json()}[id]    5
    Log To Console    ${response.json()}[name]
    Log To Console    ${response.json()}[category]
    Log To Console    ${response.json()}[category][name]

TC2 Find By Valid Status
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/findByStatus?status=sold       expected_status=200
    Log    ${response.status_code}
    Log    ${response.json()}
    Log    ${response.json()}[0]

    Log    ${response.json()}[0][status]
    Should Be Equal As Strings    ${response.json()}[0][status]    sold


TC3 Find By Valid Status
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/findByStatus?status=sold       expected_status=200
    Log    ${response.status_code}
    Log    ${response.json()}
    Log    ${response.json()}[0]
    ${size}     Get Length    ${response.json()}
    Log     ${size}

    FOR    ${i}    IN RANGE    0    ${size}
        Log    ${response.json()}[${i}][status]
        Should Be Equal As Strings    ${response.json()}[${i}][status]    sold
    END

TC4 Find By Valid Status
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/findByStatus?status=sold       expected_status=200
    Log    ${response.status_code}
    Log    ${response.json()}
    FOR    ${pet}    IN    @{response.json()}
        Log    ${pet}[status]
        Should Be Equal As Strings    ${pet}[status]    sold
    END


TC5 Find Invalid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/5000        expected_status=404
    Log    ${response.json()}
    Status Should Be    404
    Should Be Equal As Strings    ${response.json()}[message]    	Pet not found
