*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary

Test Teardown   Run Keywords    Sleep    5s     AND     Close Browser


*** Test Cases ***
TC1 Windows Credential Using URL
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    #https://username:password@URL
    Go To    url=https://admin:admin@the-internet.herokuapp.com/basic_auth
#    Sleep    5s
#    [Teardown]  Close Browser

TC2 Windows Credential Using AutoIT
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://@the-internet.herokuapp.com/basic_auth
    Sleep    2s
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {TAB}
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {ENTER}
#    Sleep    5s
#    [Teardown]  Close Browser

TC3 Upload Using AutoIT
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Click Element    id=pickfiles
    Sleep    5s
    Control Focus       Open       ${EMPTY}     Edit1
    Control Set Text    Open      ${EMPTY}      Edit1       C:${/}Automation Concepts${/}demofile.pdf
    Control Click       Open    ${EMPTY}    Button1

