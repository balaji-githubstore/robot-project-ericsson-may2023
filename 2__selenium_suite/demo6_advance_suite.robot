*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Choose File    xpath=//input[@type='file']    C:${/}Automation Concepts${/}demofile.pdf

TC2 Nasscom
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    xpath=//a[text()='Membership']
    Click Element    xpath=//a[text()='Members Listing']

TC3 Nasscom
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    xpath=//a[text()='Membership']
    Mouse Over    xpath=//a[text()='Become a Member']
    Click Element    xpath=//a[text()='Membership Benefits']

TC4 Php Option1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/
    Execute Javascript  document.querySelector('#checkin').value='19-09-2023'
    #checkin 19-08-2023
    #checkout 26-08-2023
    Execute Javascript  document.querySelector("input[name='checkout']").value='26-09-2023'

    #city - Mysore,India
    #adult 4, child 2
    #nationality - canada

TC4 Php Javascript Option2
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/
    ${ele}  Get WebElement    xpath=//input[@id='checkin']
    Execute Javascript  arguments[0].value='19-09-2023'     ARGUMENTS   ${ele} 
    
    ${checkout_ele}     Get WebElement    xpath=//input[@id='checkout']
    Execute Javascript  arguments[0].value='26-09-2023'     ARGUMENTS    ${checkout_ele}

TC5 Nasscom Js Click1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://nasscom.in/about-us/contact-us
#    Click Element    xpath=//a[text()='Members Listing']

    Execute Javascript    document.querySelector('[data-drupal-link-system-path="members-listing"]').click()

TC5 Nasscom Js Click2
    Open Browser    browser=headlesschrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://nasscom.in/about-us/contact-us
#    Click Element    xpath=//a[text()='Members Listing']
    ${ele}      Get WebElement    xpath=//a[text()='Members Listing']
    Execute Javascript    arguments[0].click()  ARGUMENTS       ${ele}
    Capture Page Screenshot
