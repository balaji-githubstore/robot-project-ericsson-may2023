*** Settings ***
Library     SeleniumLibrary


*** Test Cases ***
TC1
    Open Browser    browser=chrome
    ...  options=add_argument("--disable-notifications");add_argument("start-maximized")
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.irctc.co.in/nget/train-search
    Sleep    5s
    Close Browser

TC2 Download Dir
    &{pref}     Create Dictionary       download.default_directory=C:${/}Automation Concepts
    Open Browser    browser=chrome
    ...  options=add_experimental_option("prefs",${pref})
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.selenium.dev/downloads/
    Click Element    partial link=32
    Go To    url=chrome://downloads/
#    ${text}     Get Text    id=show
#    Log To Console    ${text}
#    Sleep    10s
#    ${res}      Execute Javascript      return document.querySelector("downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").innerText
#    Log To Console    ${res}
    ${output}   Set Variable    'Fail'
    WHILE    '${output}' == 'Fail'
          ${output}   Run Keyword And Ignore Error    Execute Javascript      return document.querySelector("downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").innerText
        IF  '${output}' == 'PASS'
             ${res}      Execute Javascript      return document.querySelector("downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").innerText
            Log To Console    ${res}
            BREAK
        END
    END
